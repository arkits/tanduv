from http.server import BaseHTTPRequestHandler
from datetime import datetime
import json

class handler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')

        to_return = {
            "name": "Tanduv",
            "version": "0.1"
        }

        self.end_headers()
        self.wfile.write((json.dumps(to_return)).encode())
        return
